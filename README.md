# Cypress demo on [TownSend](http://cafetownsend-angular-rails.herokuapp.com/)
This is a demo project done using the JavaScript testing framework Cypress.io. The following steps might be able to make this work after cloning the repository:

 1. in case you do not already have nodejs/npm installed go to [https://nodejs.org/en/](https://nodejs.org/en/)
 2. run `rm -rf node_modules && npm install`(might not be required but it does refresh all the node modules in case 3 doesn't work)
 3. open a terminal (in the project folder) and do `npx cypress open`