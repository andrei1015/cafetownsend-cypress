import * as login from '../support/Login'

describe('Login Tests', function() {
	describe('Incorrect credentials', function() {
		it('Input both credentials wrong', function() {
			cy.visit('')
			cy.get(login.usernameLocator).type('wrong-username')
			cy.get(login.passwordLocator).type('wrong-password')
			cy.get(login.loginLocator).click()
		})
		it('Check login is not performed', function() {
			cy.get(login.errorLocator).should('be.visible').and('contain', 'Invalid username or password!')
		})
		it('Input username wrong and password right', function() {
			cy.visit('')
			cy.get(login.usernameLocator).type('wrong-username')
			cy.get(login.passwordLocator).type(login.password)
			cy.get(login.loginLocator).click()
		})
		it('Check login is not performed', function() {
			cy.get(login.errorLocator).should('be.visible').and('contain', 'Invalid username or password!')
		})
		it('Input username right and password wrong', function() {
			cy.visit('')
			cy.get(login.usernameLocator).type(login.username)
			cy.get(login.passwordLocator).type('wrong-password')
			cy.get(login.loginLocator).click()
		})
		it('Check login is not performed', function() {
			cy.get(login.errorLocator).should('be.visible').and('contain', 'Invalid username or password!')
		})
	})
	describe('Correct credentials', function() {
		it('Input username right and password wrong', function() {
			cy.visit('')
			cy.get(login.usernameLocator).type(login.username)
			cy.get(login.passwordLocator).type(login.password)
			cy.get(login.loginLocator).click()
		})
		it('Check login is performed', function() {
			cy.get(login.logoutLocator).should('be.visible')
			cy.get(login.messageLocator).should('be.visible').and('contain', 'Hello Luke')
			cy.get(login.createLocator).should('be.visible')
			cy.get(login.editLocator).should('be.visible').and('have.class', 'disabled')
			cy.get(login.deleteLocator).should('be.visible').and('have.class', 'disabled')
		})
		it('Perform logout and check it is successful', function() {
			cy.get(login.logoutLocator).click()
			cy.get(login.createLocator).should('not.be.visible')
			cy.get(login.editLocator).should('not.be.visible')
			cy.get(login.deleteLocator).should('not.be.visible')
		})
	})

})