// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import * as login from '../support/Login'

Cypress.Commands.add("login", () => {
	cy.get(login.usernameLocator).should('exist').type(login.username)
	cy.get(login.passwordLocator).should('exist').type(login.password)
	cy.get(login.loginLocator).should('exist').click()
	cy.get(login.logoutLocator).should('be.visible')
	cy.get(login.messageLocator).should('be.visible').and('contain', 'Hello Luke')
	cy.get(login.createLocator).should('be.visible')
	cy.get(login.editLocator).should('be.visible').and('have.class', 'disabled')
	cy.get(login.deleteLocator).should('be.visible').and('have.class', 'disabled')
})